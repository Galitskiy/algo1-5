﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort
{
    public class Program
    {
        //вибором
        public static List<int> SelectionSort(List<int> mas)
        {
            int indexMin;
            var sortedMas = mas;
            for (int i = 0; i < sortedMas.Count; i++)
            {
                indexMin = i;
                for (int j = i + 1; j < sortedMas.Count; j++)
                {
                    if (sortedMas[j] < sortedMas[indexMin])
                        indexMin = j;
                }
                if (indexMin != i)
                    swap(i, indexMin, sortedMas);
            }

            return sortedMas;
        }
       //вставками
        public static List<int> InsertionSort(List<int> mas)
        {
            var sortedMas = mas;

            for (int i = 1; i < sortedMas.Count; i++)
            {
                
                for (int j = i; j >= 1 ; j--)
                {
                    if (sortedMas[j - 1] > sortedMas[j])
                        swap(j - 1, j, sortedMas);
                    else break;
                }                
            }

            return sortedMas;
        }
        //вставками
        public static LinkedList<int> InsertionSortForList(LinkedList<int> list)
        {
            var sortedList = list;
            LinkedListNode<int> current = sortedList.First;
            var localCurrent = current;
            for (int i = 1; i < list.Count; i++)           
            {
                current = current.Next;
                localCurrent = current;
                for (var j = i; j >= 1; j--)
                {                   
                    if (localCurrent.Value < localCurrent.Previous.Value)
                    {
                        int temp = localCurrent.Value;
                        localCurrent.Value = localCurrent.Previous.Value;
                        localCurrent.Previous.Value = temp;

                        localCurrent = localCurrent.Previous;
                    }                        
                    else break;
                }
                
            }

            return sortedList;
        }
        //сортировка шелла
        public static List<int> ShellSort(List<int> mas)
        {
            var sortedMas = mas;

            int step = sortedMas.Count / 2;
            while (step != 0)
            {
                for (int i = step; i < sortedMas.Count; i++)
                {

                    for (int j = i; j >= step; j -= step)
                    {
                        if (sortedMas[j - step] > sortedMas[j])
                            swap(j - step, j, sortedMas);
                        else break;
                    }
                }
                step /= 2;
            }

            return sortedMas;
        }

        public static void swap(int index, int indexMin, List<int> mas)
        {
            int temp = mas[index];
            mas[index] = mas[indexMin];
            mas[indexMin] = temp;
        }
                
        static void Main(string[] args)
        {
            var rnd = new Random((int)DateTime.Now.Ticks);

            while (true)
            {
                var list = new List<int>();
                for (int i = 0; i < 10; i++)
                {
                    list.Add(rnd.Next(0, 10));
                    
                }

                var sortedMas = ShellSort(list);

                foreach (int item in sortedMas)
                    Console.Write(item);

                Console.ReadLine();
            }
        }
    }
}

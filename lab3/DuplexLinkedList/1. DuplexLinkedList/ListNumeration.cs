﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuplexLinkedList
{
    class ListNumeration<T> : IEnumerator<T>
    {
        D<T> DList;

        public ListNumeration(D<T> list)
        {
            DList = list; 
        }

        public SomeListItem<T> currentElem;        

        public T Current => currentElem.data;

        object IEnumerator.Current => throw new NotImplementedException();

        public void Dispose()
        {
            DList = null;
        }

        public bool MoveNext()
        {
            if(currentElem == null)
            {
                currentElem = DList.head;
                return true;

            }

            if (currentElem.next != null)
            {                
                currentElem = currentElem.next;
                return true;
            }
            else return false;
        }

        public void Reset()
        {
            currentElem = DList.head;
            
        }
    }

    //class ListEnumerator<T> : IEnumerator<ListItem<T>>
    //{
    //    DuplexList<T> DList;

    //    public ListEnumerator(DuplexList<T> list)
    //    {
    //        DList = list;
    //        current = DList.head;
    //    }

    //    public ListItem<T> current;

    //    public ListItem<T> Current
    //    {
    //        get
    //        {
    //            return current;
    //        }
    //    }

    //    object IEnumerator.Current => throw new NotImplementedException();

    //    public void Dispose()
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public bool MoveNext()
    //    {
    //        if (current.next != null)
    //        {
    //            current = current.next;
    //            return true;
    //        }
    //        else return false;
    //    }

    //    public void Reset()
    //    {
    //        current = DList.head;
    //    }
    //}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuplexLinkedList
{
    class SomeListItem<T>
    {
        public SomeListItem<T> next;
        public SomeListItem<T> previous;

        public T data = default(T);

        public SomeListItem(T data)
        {
            this.data = data;
        }

        public override string ToString()
        {
            return data.ToString();
        }
    }
}

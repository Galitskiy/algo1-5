﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuplexLinkedList
{
    class MyProgram
    {
        public static double PolishCalculate(string str)
        {
            var stack = new Stack<double>();

            string[] strMas = str.Split(' ');
            
            foreach (string item in strMas)
            {
                double second;
                double first;                

                switch (item)
                { 
                    case ("+"):                        
                        stack.Push(stack.Pop() + stack.Pop());
                        break;

                    case ("-"):
                        stack.Push(-(stack.Pop() - stack.Pop()));
                        break;

                    case ("*"):
                        stack.Push(stack.Pop() * stack.Pop());
                        break;

                    case ("/"):
                        stack.Push(Math.Pow(stack.Pop() / stack.Pop(), -1));
                        break;

                    case ("^"):
                        second = stack.Pop();
                        first = stack.Pop();
                        stack.Push(Math.Pow(first, second));
                        break;

                    case ("sqrt"):                       
                        stack.Push(Math.Sqrt(stack.Pop()));
                        break;

                    default:
                        if(double.TryParse(item,out double value))
                            stack.Push(value);
                        break;
                }
            }

            return stack.Pop();
        }

        static void Main(string[] args)
        {

            var DL = new D<int>();

            for (int i = 0; i < 5; i++)
            {
                DL.Addend(i);
            }

            DL.printList();
            Console.WriteLine((new string('-',49)));
            Console.WriteLine(PolishCalculate("7 5 2 - 4 * + sqrt"));

            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace lab2
{

    class MyRandom
    {
        int a = 214013,
            c = 2531011;

        public long nextResult ;
        long m = 4294967296;
        

        public long next(int min = int.MinValue, int max = int.MaxValue)
        {
            nextResult = ((a * nextResult + c) % m);
            return Math.Abs(nextResult) % (max + 1) - min;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            DateTime date = DateTime.Now;

            MyRandom rand = new MyRandom();
            rand.nextResult = date.Millisecond;

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(rand.next(0, 10));
            }

            PrintInfo(rand, 0, 100, 5000);
            ReadLine();
        }

        static void PrintInfo(MyRandom rand, int min, int max, int count)
        {
            int index = 0;
            List<long> list = new List<long>(max + 1);

            for (int i = 0; i < max + 1; i++)
            {
                list.Add(0);
            }

            for (int i = 0; i < count; i++)
            {
                list[(int)rand.next(min, max)]++;
            }

            WriteLine("Кiлькiсть появ 1: " + list[1]);

            WriteLine("Частота появи 1: " + (list[1] / (double)count));

            double mathMet = list.Sum(item => item * index++) / (double)count;
            WriteLine("Математичне сподiвання: " + mathMet);

            double disp = 0;
            index = 0;
            foreach (long item in list)
            {
                disp += Math.Pow(mathMet - index++, 2) * item;
            }
            disp /= count;
            WriteLine("Дисперсiя: " + disp);

            WriteLine("Середньоквадритичне вiдхилення: " + Math.Sqrt(disp));
        }
    }
}